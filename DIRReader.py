#!/usr/bin/env python3

import os
import numpy as np
from glob import glob

class DIRReader():
    def get_data_type(self, name):
      if name == 'Real' or name == 'GlobalData_t':
        return 'float'
      if name == 'Gatetype':
        return 'float32'

    def read(self, basename, mf=None, data_set_ID=None):
        files = glob(basename+'*')

        data_list = []
        #datasize_list = []
        datatype_list = []
        #timesteps = 0
        data_dir = None

        for file in files:
          if file.endswith("_header.txt"):
            with open(file) as headerfile:
                line_idx = 1
                for line in headerfile:
                  if line_idx > 1:
                    #line = line.lstrip()
                    ll = line.split()
                    data_list     += [ll[0]]
                    datatype_list += [ll[1]]
                    #datasize_list += [ll[2]]
                    #timesteps = float(ll[3])
                  line_idx += 1
            data_dir = os.path.dirname(file)
            break;

        fd = {}
        ff = {data_set_ID: fd}

        data_idx = 0
        for file in data_list:
            file = os.path.join(data_dir, file)

            if file.endswith(".bin"):
              name = file.split('.')[-2]
            else:
              name = file.split('.')[-1]

            datatype = self.get_data_type(datatype_list[data_idx])

            fd[name] = np.fromfile(file, datatype)
            data_idx += 1

        return ff


