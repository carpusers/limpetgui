#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# Bernardo M. Rocha, 2014
# Gilmar F. S. Filho

import sys
import getopt
from PyQt5.QtWidgets import QApplication

from limpet import MainWindow
import util
from DataReader import DataReader
import argparse

def parser():

    parser = argparse.ArgumentParser( description = 'Parse limpetgui input parameters')

    parser.add_argument('rfile', type=argparse.FileType('r'), nargs=1,
                        help="input h5 reference file to visualize")
    parser.add_argument('cfile', type=argparse.FileType('r'), nargs='?',
                        help="optional input h5 file to compare against reference")
    parser.add_argument('-o', '--offset', type=float, default=0.,
                        help='trace offset in time of compared relative to reference file ')
    #parser.add_argument('--vspacing', type=int, default=0, help='control vertical space between plots')
    #parser.add_argument('--hspacing', type=int, default=0, help='control horizontal space between plots')
    parser.add_argument('-m', '--map', type=argparse.FileType('r'), default=None, help='mapping file')
    parser.add_argument('-f', '--filter', type=str, default=None,
                        help='filter listed state variables')
    parser.add_argument('-e', '--excl', action='store_true',
                        help='invert filter, exclude listed state variables')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    return parser


def file_chk(file1,file2):

    ok = True

    if not util.check_file_extension(file1):
        return not ok

    if file2:
        if not util.check_file_extension(file2):
            return not ok

    return ok


def main():

    # parse options
    args = sys.argv[1:]

    # no args given, start GUI only
    if len(args) == 0:
        app = QApplication(sys.argv)
        ex = MainWindow([], [None, None], [None, None])
        ex.show()
        sys.exit(app.exec_())

    # parse command line
    p = parser()
    p_args = p.parse_args(args)

    # store file names of reference, comparison and map file
    file1 = p_args.rfile[0].name
    p_args.rfile[0].close()

    file2 = None
    if p_args.cfile is not None:
        file2 = p_args.cfile.name
        p_args.cfile.close()

    if p_args.map is not None:
        mapfile = p_args.map.name

    if p_args.filter is not None:
        inclist = p_args.filter.split(',')

    # check file extensions
    if not file_chk(file1,file2):
        sys.exit(1)

    # reading data
    mf, ff1, ff2 = None, None, None
    groups = [None, None]

    # read in map
    mf = util.read_mapfile(p_args.map)

    # read in main h5 data file
    print("Reading  data from file: %s\n" % file1)

    datafile1 = DataReader.factory(util.get_file_extension(file1))
    groups[0] = 'protocol_1'
    ff1 = datafile1.read(file1, mf, groups[0])
    svlist1 = svlist = util.get_svlist(ff1, groups[0])
    if p_args.verbose:
        util.print_svlist(svlist,groups[0])

    # data to compare
    if file2 is not None:
        print("Reading data from file: %s\n" % file2)
        datafile2 = DataReader.factory(util.get_file_extension(file2))
        groups[1] = 'protocol_1'
        ff2 = datafile2.read(file2, mf, groups[1])
        svlist2 = util.get_svlist(ff2, groups[1])
        if p_args.verbose:
            util.print_svlist(svlist2, groups[1])

        # Try to match datasets
        shared_list, rkeys = util.match_sv_lists(svlist1,svlist2,mf)

        # rename keys now using rkeys list
        for rkey,rval in rkeys.items():
            if rkey in ff1[groups[0]].keys():
                ff1[groups[1]][rval] = ff1[groups[1]].pop(rkey)

            if rkey in ff2[groups[1]].keys():
                ff2[groups[1]][rval] = ff2[groups[1]].pop(rkey)

        # remove any traces not listed in the shared_list
        rm_keys = []
        for key in ff1[groups[0]].keys():
            if not key in shared_list:
                rm_keys.append(key)

        for key in rm_keys:
            print('Removing trace %s from dataset %s'%(key,groups[0]))
            ff1[groups[0]].pop(key)

        rm_keys.clear()
        for key in ff2[groups[1]].keys():
            if not key in shared_list:
                rm_keys.append(key)

        for key in rm_keys:
            print('Removing trace %s from dataset %s' % (key,groups[1]))
            ff2[groups[1]].pop(key)

        svlist = list(sorted(shared_list.keys()))

    # filter inclusion list
    if p_args.filter is not None:
        # replace filter keys with map dictionary first
        # and create filtered state variable and removal list
        rm_list, svlist = util.filter_svs(svlist,p_args.filter,mf,not p_args.excl)

        # remove all svs in removal list from  data
        for rkey in rm_list:
            ff1[groups[0]].pop(rkey)

            if file2 is not None:
                ff2[groups[1]].pop(rkey)


    # remove independent variable used for abscissa
    t_name, svlist = util.rm_time_from_svlist(svlist)

    # shift time axis of compare trace relative to reference by offset
    if file2 is not None:
        ff2[groups[1]][t_name] = ff2[groups[1]][t_name] + p_args.offset

    # final steps
    app = QApplication(sys.argv)
    data = [ff1, ff2]

    ex = MainWindow(svlist, data, groups)
    ex.show()

    sys.exit(app.exec_())
# ------------------------------------------------------------------------------

if __name__ == '__main__':
    main()
