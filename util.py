# -*- coding: utf-8 -*-
# Bernardo M. Rocha, 2014
# Gilmar F. S. Filho

time_names = ('t', 'T', 'time', 'Time', 'TIME')
pot_names = ('POTENTIAL', 'Potential', 'potential', 'V', 'v', 'Vm', 'vm', 'VM')
file_exts = ['.h5', '.csv', '.txt', '.xls']


def search_var(file, var, group):
    var_names = {'time' : time_names, 'potential' : pot_names}
    for k in file[group].keys():
        k = str(k)
        if k in var_names[var]:
            return k

    print("Error: variable %s was not found." % var)
    exit(1)


def get_file_extension(file):
    extension = None
    if file.endswith('.h5'):
        extension = '.h5'
    elif file.endswith('.csv'):
        extension = '.csv'
    elif file.endswith('.txt'):
        extension = '.txt'
    elif file.endswith('.xls'):
        extension = '.xls'
    return extension


def check_file_extension(file):
    extension = get_file_extension(file)
    if extension is None:
        print("\nError: file extension of %s not supported." %(file))
        supported_exts = ','.join(file_exts)
        print('Supported extensions are: %s\n'%(supported_exts))
        return False
    return True


def get_svlist(f, group):

    return list(sorted(f[group].keys()))

def print_svlist(svlist, label):

    print('\nVariables found in %s'%(label))
    print('--------------------------------------\n')

    for tag in svlist:
        print(tag)

    print('\n\n')

def rm_time_from_svlist(svlist):

    # remove time variable from state variable list
    for tag in svlist:
        if tag in time_names:
            time_var = tag
            svlist.remove(tag)
            break

    return time_var, svlist


def replace_keys(klist, mf):

    rkey_list = {}
    for key, val in mf.items():
        if val in klist:
            klist.pop(val)
            klist[key] = key
            rkey_list[val] = key

    return rkey_list, klist

def sv_list_to_dict(svlist):

    klist = {}
    for key in svlist:
        klist[key] = key

    return klist

def match_sv_lists(svlist1,svlist2,mf):

    # no map provided, pick only state variables of the same name
    # ignore the rest, map mf is empty in this case
    if mf is None:
        mf = {}

    # turn lists into dicts
    key_list_1 = sv_list_to_dict(svlist1)
    key_list_2 = sv_list_to_dict(svlist2)

    # replace keys showing up in map
    rkey_list = {}
    rkey_list_1, key_list_1 = replace_keys(key_list_1,mf)
    rkey_list_2, key_list_2 = replace_keys(key_list_2,mf)
    rkey_list.update(rkey_list_1)
    rkey_list.update(rkey_list_2)

    # create unique key list
    shared_keys = key_list_2.copy()
    shared_keys.update(key_list_1)

    # remove non-shared keys
    non_shared_keys = []
    for key in shared_keys:
        if not ( key in key_list_1 and key in key_list_2):
            non_shared_keys.append(key)
    for key in non_shared_keys:
        shared_keys.pop(key)

    return shared_keys, rkey_list


def filter_svs(svlist, filter, mf, incl):

    rm_list = []
    f_list = filter.split(',')

    # map labels
    for key,val in mf.items():
        if val in f_list:
            f_list[f_list.index(val)] = key

    if incl:
        print('Processing inclusion filter: {}'.format(f_list))
        # keep also time variable in
        f_list = f_list + list(time_names)

        for key in svlist:
            if key not in f_list:
                rm_list.append(key)
    else:
        print('Processing exclusion filter: {}'.format(f_list))
        for key in svlist:
            if key in f_list:
                rm_list.append(key)

    for rkey in rm_list:
        svlist.remove(rkey)

    return rm_list, svlist


def read_mapfile(fmap):
    mf = {}

    if fmap is not None:
        print('Reading map file %s'%fmap.name)
        l = fmap.readline()
        while l:
            l = l.replace(" ", "")
            var1, var2 = l.split(',')
            var2 = var2[:len(var2)-1]
            mf[var2] = var1
            l = fmap.readline()

        fmap.close()

    return mf


def map_variables(ff2, sv1, sv2, mf, group):
    sv2 = sv1

    for key in ff2[group].keys():
        if key in mf.keys():
            ff2[group][mf[key]] = ff2[group].pop(key)
